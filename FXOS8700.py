#!/usr/bin/env python
# -*- coding: utf-8 -*-

from contextlib import contextmanager


class FXOS8700:
    # The I2C slave addresses that may be assigned to the FXOS8700CQ part
    # depends on the logic level of the SA1 and SA0 inputs
    # SA1=0 & SA0=0 => Slave address = 0x1E
    # SA1=0 & SA0=1 => Slave address = 0x1D
    # SA1=1 & SA0=0 => Slave address = 0x1C
    # SA1=1 & SA0=1 => Slave address = 0x1F
    FXOS8700_ADDR   = 0x1F
    FXOS8700_ID     = 0xC7

    # FXOS8700 registers
    FXOS8700_REG_STATUS         = 0x00
    FXOS8700_REG_OUT_X_MSB      = 0x01
    FXOS8700_REG_OUT_X_LSB      = 0x02
    FXOS8700_REG_OUT_Y_MSB      = 0x03
    FXOS8700_REG_OUT_Y_LSB      = 0x04
    FXOS8700_REG_OUT_Z_MSB      = 0x05
    FXOS8700_REG_OUT_Z_LSB      = 0x06
    FXOS8700_REG_WHO_AM_I       = 0x0D
    FXOS8700_REG_XYZ_DATA_CFG   = 0x0E
    FXOS8700_REG_CTRL_REG1      = 0x2A
    FXOS8700_REG_CTRL_REG2      = 0x2B
    FXOS8700_REG_CTRL_REG3      = 0x2C
    FXOS8700_REG_CTRL_REG4      = 0x2D
    FXOS8700_REG_CTRL_REG5      = 0x2E
    FXOS8700_REG_MSTATUS        = 0x32
    FXOS8700_REG_MOUT_X_MSB     = 0x33
    FXOS8700_REG_MOUT_X_LSB     = 0x34
    FXOS8700_REG_MOUT_Y_MSB     = 0x35
    FXOS8700_REG_MOUT_Y_LSB     = 0x36
    FXOS8700_REG_MOUT_Z_MSB     = 0x37
    FXOS8700_REG_MOUT_Z_LSB     = 0x38
    FXOS8700_REG_MCTRL_REG1     = 0x5B
    FXOS8700_REG_MCTRL_REG2     = 0x5C
    FXOS8700_REG_MCTRL_REG3     = 0x5D

    def __init__(self, bus):
        self._bus = bus

        self.status = 0
        self.accelRawX = 0
        self.accelRawY = 0
        self.accelRawZ = 0
        self.magRawX = 0
        self.magRawY = 0
        self.magRawZ = 0

        # Make sure the chip ID is correct
        chipId = self.read8(self.FXOS8700_REG_WHO_AM_I)
        if chipId != self.FXOS8700_ID:
            raise TypeError("The chip ID (0x{:02X}) do not match "
                            "the FXOS8700CQ one (0x{:02X}) !"
                            "".format(chipId, self.FXOS8700_ID))

        # Place the part into standby mode (required for changes)
        self.write8(self.FXOS8700_REG_CTRL_REG1, 0)

        ###############################
        # Configure the accelerometer #
        ###############################
        # Range +/- 2g
        self.write8(self.FXOS8700_REG_XYZ_DATA_CFG, 0x00)
        # High resolution
        self.write8(self.FXOS8700_REG_CTRL_REG2, 0x02)
        # Active, Normal mode, Low noise, 100Hz in hybrid mode
        self.write8(self.FXOS8700_REG_CTRL_REG1, 0x15)

        ############################
        # Configure the magnometer #
        ############################
        # Hybrid mode, Over sampling rate = 16
        self.write8(self.FXOS8700_REG_MCTRL_REG1, 0x1F)
        # Jump to reg 0x33 after reading 0x06
        self.write8(self.FXOS8700_REG_MCTRL_REG2, 0x20)

    def write8(self, address, value):
        self._bus.write_byte_data(self.FXOS8700_ADDR, address, value)

    def read8(self, address):
        return self._bus.read_byte_data(self.FXOS8700_ADDR, address)

    def readMultipleBytes(self, address, nbBytes):
        if nbBytes < 2:
            return self.read8(address)
        else:
            return self._bus.read_i2c_block_data(self.FXOS8700_ADDR,
                                                 address,
                                                 nbBytes)
    def _bytesToWord(self, msb, lsb):
        return (msb << 8) | lsb

    def readState(self):
        self.status = 0
        self.accelRawX = 0
        self.accelRawY = 0
        self.accelRawZ = 0
        self.magRawX = 0
        self.magRawY = 0
        self.magRawZ = 0

        data = self.readMultipleBytes(self.FXOS8700_REG_STATUS, 13)
        self.status = data[0]
        # Accelerometer data resolution on full scale range is 14 bits
        self.accelRawX = self._bytesToWord(data[1], data[2]) >> 2
        self.accelRawY = self._bytesToWord(data[3], data[4]) >> 2
        self.accelRawZ = self._bytesToWord(data[5], data[6]) >> 2
        # Magnometer data resolution is 16 bits
        self.magRawX = self._bytesToWord(data[7], data[8])
        self.magRawY = self._bytesToWord(data[9], data[10])
        self.magRawZ = self._bytesToWord(data[11], data[12])


@contextmanager
def FXOS8700Wrapper(bus):
    fxos = FXOS8700(bus)
    yield fxos
    # Place the part into standby mode
    fxos.write8(fxos.FXOS8700_REG_CTRL_REG1, 0)

if __name__ == '__main__':
    from datetime import datetime
    from smbus2 import SMBusWrapper

    with SMBusWrapper(2) as bus, FXOS8700Wrapper(bus) as fxos:
        start_t = datetime.now()
        for i in range(100):
            sample_t = (datetime.now() - start_t).total_seconds()
            fxos.readState()
            if fxos.accelRawX or fxos.accelRawY or fxos.accelRawZ \
                    or fxos.magRawX or fxos.magRawY or fxos.magRawZ:
                print "{:0.6f}: {:08b} - {:5d} - {:5d} - {:5d} - {:5d} - {:5d} - {:5d}".format(sample_t,
                                                     fxos.status,
                                                     fxos.accelRawX,
                                                     fxos.accelRawY,
                                                     fxos.accelRawZ,
                                                     fxos.magRawX,
                                                     fxos.magRawY,
                                                     fxos.magRawZ)

