#!/usr/bin/env python
# -*- coding: utf-8 -*-

from contextlib import contextmanager

class ADXL345():
    ADXL345_ADDR = 0x53
    ADXL345_ID   = 0xE5

    # ADXL345 registers
    ADXL345_REG_DEVID               = 0x00
    ADXL345_REG_THRESH_TAP          = 0x1D
    ADXL345_REG_OFSX                = 0x1E
    ADXL345_REG_OFSY                = 0x1F
    ADXL345_REG_OFSZ                = 0x20
    ADXL345_REG_DUR                 = 0x21
    ADXL345_REG_LATENT              = 0x22
    ADXL345_REG_WINDOW              = 0x23
    ADXL345_REG_THRESH_ACT          = 0x24
    ADXL345_REG_THRESH_INACT        = 0x25
    ADXL345_REG_TIME_INACT          = 0x26
    ADXL345_REG_ACT_INACT_CTL       = 0x27
    ADXL345_REG_THRESH_FF           = 0x28
    ADXL345_REG_TIME_FF             = 0x29
    ADXL345_REG_TAP_AXES            = 0x2A
    ADXL345_REG_ACT_TAP_STATUS      = 0x2B
    ADXL345_REG_BW_RATE             = 0x2C
    ADXL345_REG_POWER_CTL           = 0x2D
    ADXL345_REG_INT_ENABLE          = 0x2E
    ADXL345_REG_INT_MAP             = 0x2F
    ADXL345_REG_INT_SOURCE          = 0x30
    ADXL345_REG_DATA_FORMAT         = 0x31
    ADXL345_REG_DATAX0              = 0x32
    ADXL345_REG_DATAX1              = 0x33
    ADXL345_REG_DATAY0              = 0x34
    ADXL345_REG_DATAY1              = 0x35
    ADXL345_REG_DATAZ0              = 0x36
    ADXL345_REG_DATAZ1              = 0x37
    ADXL345_REG_FIFO_CTL            = 0x38
    ADXL345_REG_FIFO_STATUS         = 0x39

    def __init__(self, bus):
        self._bus = bus

        self.rawX = 0
        self.rawY = 0
        self.rawZ = 0

        # Make sure the chip ID is correct
        chipId = self.read8(self.ADXL345_REG_DEVID)
        if chipId != self.ADXL345_ID:
            raise TypeError("The chip ID (0x{:02X}) do not match "
                            "the ADXL345 one (0x{:02X}) !"
                            "".format(chipId, self.ADXL345_ID))

        # Place the part into measurement mode
        self.write8(self.ADXL345_REG_POWER_CTL, 0x08)
        self.setDataFormat()

    def read8(self, address):
        return self._bus.read_byte_data(self.ADXL345_ADDR, address)

    def write8(self, address, value):
        self._bus.write_byte_data(self.ADXL345_ADDR, address, value)

    def _bytesToWord(self, msb, lsb):
        return (msb << 8) | lsb

    def setDataFormat(self, gRange=0, fullRes=True):
        fmt = gRange
        fmt |= fullRes << 3
        self.write8(self.ADXL345_REG_DATA_FORMAT, fmt)

    def readState(self):
        data = self._bus.read_i2c_block_data(self.ADXL345_ADDR, self.ADXL345_REG_DATAX0, 6)
        self.rawX = self._bytesToWord(data[1], data[0])
        self.rawY = self._bytesToWord(data[3], data[2])
        self.rawZ = self._bytesToWord(data[5], data[4])


@contextmanager
def ADXL345Wrapper(bus):
    adxl = ADXL345(bus)
    yield adxl
    # Place the part into standby mode
    adxl.write8(adxl.ADXL345_REG_POWER_CTL, 0)


if __name__ == '__main__':
    from datetime import datetime
    from smbus2 import SMBusWrapper

    with SMBusWrapper(2) as bus, ADXL345Wrapper(bus) as adxl:
        start_t = datetime.now()
        for i in range(1000):
            sample_t = datetime.now() - start_t
            adxl.readState()
            if adxl.rawX or adxl.rawY or adxl.rawZ:
                print "{:0.6f}: {:5d} - {:5d} - {:5d}".format(sample_t.total_seconds(),
                                                              adxl.rawX,
                                                              adxl.rawY,
                                                              adxl.rawZ)

