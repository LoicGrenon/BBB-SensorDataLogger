#!/usr/bin/env python
# -*- coding: utf-8 -*

from ADXL345 import ADXL345Wrapper
from contextlib import contextmanager
import csv
from datetime import datetime
from smbus import SMBusWrapper()

@contextmanager
def LoggerDevice(i2cId, deviceWrapper):
    with SMBusWrapper(i2cId) as bus, deviceWrapper(bus) as device:
        yield device


if __name__ == '__main__':
    import sys

    try:
        maxSampleTime = float(sys.argv[1])
    except (IndexError, ValueError):
        maxSampleTime = 5.0
    if maxSampleTime > 60:
        raise ValueError("Maximum sample time allowed is 60s.")

    filename = "logger_{}.csv".format(datetime.now().strftime("%Y%m%d_%H%M%S%f"))
    with LoggerDevice(2, ADXL345Wrapper) as dev, open(filename, 'wb') as f:
        writer = csv.writer(f)
        start_t = datetime.now()
        sample_t = (start_t - start_t).total_seconds()
        i = 0
        while sample_t < maxSampleTime:
            sample_t = (datetime.now() - start_t).total_seconds()
            dev.readState()
            if dev.rawX or dev.rawY or dev.rawZ:
                print "{:0.6f}: {:5d} - {:5d} - {:5d}".format(sample_t,
                                                              dev.rawX,
                                                              dev.rawY,
                                                              dev.rawZ)
            writer.writerow([i, 0, dev.rawX, dev.rawY, dev.rawZ])
            i += 1
    print "{} records in {}s ({:0.3f} ms/sample)".format(i,
                                                         sample_t,
                                                         100 * sample_t / i)

