#!/usr/bin/env python
# -*- coding: utf-8 -*

from ADXL345 import ADXL345
from datetime import datetime
from smbus2 import SMBusWrapper
import sqlite3


class DataLogger():
    def __init__(self):
        self.db = sqlite3.connect("logger_{}.db".format(datetime.now().strftime("%Y%m%d_%H%M%S%f")))
        self._commitCnt = 0
        self._createTable()

    def _createTable(self):
        self.execute("""
            CREATE TABLE `logger` (
                `PK_I_event` integer PRIMARY KEY AUTOINCREMENT,
                `I_EventTimeStamp` integer NOT NULL,
                `I_X_ACC` integer NOT NULL,
                `I_Y_ACC` integer NOT NULL,
                `I_Z_ACC` integer NOT NULL)""")
        self.commit()

    def insert(self, t, x, y, z):
        r = "INSERT INTO `logger` (`I_EventTimeStamp`, `I_X_ACC`, `I_Y_ACC`, `I_Z_ACC`) VALUES (?, ?, ?, ?)"
        self.execute(r, (t, x, y, z))

        if self._commitCnt % 5000 == 0:
            self.commit()

    def execute(self, r, v=()):
        self.db.cursor().execute(r, v)
        self._commitCnt += 1

    def commit(self):
        self.db.commit()
        self._commitCnt = 0

    def close(self):
        self.commit()
        self.db.close()


if __name__ == '__main__':
    import sys

    try:
        maxSampleTime = float(sys.argv[1])
    except (IndexError, ValueError):
        maxSampleTime = 5.0
    if maxSampleTime > 60:
        raise ValueError("Maximum sample time allowed is 60s.")

    d = DataLogger()
    with SMBusWrapper(2) as bus:
        adxl = ADXL345(bus)
        start_t = datetime.now()
        sample_t = (start_t - start_t).total_seconds()
        i = 0
        while sample_t < maxSampleTime:
            sample_t = (datetime.now() - start_t).total_seconds()
            adxl.readState()
            if adxl.rawX or adxl.rawY or adxl.rawZ:
                print "{:0.6f}: {:5d} - {:5d} - {:5d}".format(sample_t,
                                                              adxl.rawX,
                                                              adxl.rawY,
                                                              adxl.rawZ)
            d.insert(0, adxl.rawX, adxl.rawY, adxl.rawZ)
            i += 1
        print "{} records in {}s ({:0.3f} ms/sample)".format(i,
                                                             sample_t,
                                                             100 * sample_t / i)
        d.close()
        # Place the part into standby mode
        adxl.writeRegister(adxl.POWER_CTL, 0)

